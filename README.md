# FOSDEM 2024 In Person -- Translations DevRoom Call for Participation!

[FOSDEM 2024](https://fosdem.org/2024) will take place on Saturday 3rd and Sunday 4th February 2024 in Brussels, Belgium.

## Key dates / New updates

* Conference dates: 3-4 February, 2024 In person.
* [Translations DevRoom](https://fosdem.org/2024/schedule/track/translations/) date: Sunday 4th February (half a day in the morning).
* Submission deadline: Sunday 10th December.
* Announcement of selected talks: Friday 15th December.
* You must be available in person to present your talk.
* **Talk submissions should be 30 mins and this should include Q&A time.**
    
## TALK TOPICS

For the second year in a row, FOSDEM will hold a devroom on all things related to translating FLOSS projects.

* Recruiting new people to translate your project.
* How it works in your project to translate softwares, websites, packages, manuals, tools, and so on.
* Which tools are used by translators.
* Exchange experiences between FLOSS projects.
* Rewarding volunteer contributors and keeping volunteers motivated.
* Showing which languages your project has been translated to.

Again, these are just suggestions. We welcome proposals on any aspect of translations!

## HOW TO SUBMIT A TALK

* Visit the [FOSDEM 2024 pretalx](https://pretalx.fosdem.org/fosdem-2024/cfp) website. _This has replaced Pentabarf!_
* Follow the instructions to create an account.
* Once logged in, select “submit a CFP”.
* Your submission must include the following information:
    - Proposal title
    - Track - Select **Translations** from the drop down list 
    - Abstract
    - Description
    - Add any other details as needed to the Submission notes.
    - Additional speaker
    - _Click Continue or you can save and come back to it_
    - About your proposal: Which open source license do you use?
       - FOSDEM is an open-source software conference, please specify which [OSI approved license](https://opensource.org/licenses/) your proposal uses.
    - Extra review material, only shown to reviewers (not published when scheduled)
    - Extra Contact details (optional)
    - _Click Continue or you can save and come back to it_
    - Finally tell us your name 
    - Biography 
    - Availability

If you need to get in touch with the organiser of the Translations DevRoom, email us at <translations-devroom-manager@fosdem.org>

FOSDEM [code of conduct](https://fosdem.org/2024/practical/conduct)

[Paulo Henrique de Lima Santana](https://phls.com.br) - Translations DevRoom Organiser
